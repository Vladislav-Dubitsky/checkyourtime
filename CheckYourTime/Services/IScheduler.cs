﻿using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckYourTime.Services
{
    public interface IScheduler
    {
        Registry SetSchedulerAsync();
        Task ExecuteScheduleAsync();
    }
}
