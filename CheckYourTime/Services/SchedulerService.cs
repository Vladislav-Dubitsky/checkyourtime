﻿using AngleSharp.Parser.Html;
using FluentScheduler;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckYourTime.Services
{
    public class SchedulerService :IScheduler
    {
        private readonly IConfiguration configuration;
        private readonly IMailService mailService;
        private readonly IParser parser;

        private Dictionary<string, DateTime> teamDates;
        public SchedulerService(IConfiguration configuration, IMailService mailService, IParser parser)
        {
            this.parser = parser;
            this.configuration = configuration;
            this.mailService = mailService;
        }
        public async Task ExecuteScheduleAsync()
        {
            teamDates = new Dictionary<string, DateTime>();
            var result = await parser.LogInAsync();
            if (result != null)
            {
                var parser = new HtmlParser();
                var document = parser.Parse(result);
                var team = document.QuerySelectorAll("div#mwAll");
                foreach (var parent in team)
                {
                    foreach (var child in parent.Children)
                    {
                        if (!String.IsNullOrEmpty(child.FirstElementChild.InnerHtml))
                        {
                            Console.WriteLine(child.FirstElementChild.InnerHtml);
                            teamDates.Add(child.LastElementChild.InnerHtml, Convert.ToDateTime(child.FirstElementChild.InnerHtml));
                        }
                    }
                }
            }
            await parser.CheckTime(teamDates, Startup.Members);
        }

        public Registry SetSchedulerAsync()
        {
            var register = new Registry();
            register.Schedule(async () => await ExecuteScheduleAsync()).ToRunEvery(0).Weeks().On(DayOfWeek.Wednesday).At(18, 00);
            register.Schedule(async () => await ExecuteScheduleAsync()).ToRunEvery(0).Weeks().On(DayOfWeek.Friday).At(18, 00);
            return register;
        }
    }
}
