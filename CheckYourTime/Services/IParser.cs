﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckYourTime.Services
{
    public interface IParser
    {
        Task<string> LogInAsync();
        Task CheckTime(IDictionary<string, DateTime> teamDates, IDictionary<string,string> members);
    }
}
