﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CheckYourTime.Services
{
    public class MailService: IMailService
    {
        private readonly IConfiguration configuration;
        public MailService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task SendAsync(string toEmailAddress, string subject, string body)
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(configuration["SMTP:Email"],"TATEEDA INC ©");
            mail.To.Add(toEmailAddress);
            mail.Subject = subject;
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
            var imageData = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAMQAAAAdCAMAAADCSTO+AAABR1BMVEUAAAAAAAAAAAAAAAA+tfAAAAAAAAAAAAAAAAAAAAAAAAA+tfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+tfAAAAAAAAAAAAA+tfAAAAA+tfAAAAA+tfA+tfAAAAA+tfAAAAADCg4+tfAAAAAAAAA+tfAAAAAAAAAAAAAAAAA+tfAAAAA+tfA+tfAAAAA+tfA+tfA+tfA+tfAAAAA+tfAAAAA+tfA+tfAAAAAAAAAAAAAAAAAAAAAAAAA+tfAAAAAAAAAAAAAAAAA+tfE+tfAAAAAAAAA+tfA+tfA+tvEAAAAAAAAAAAA+tfAAAAA+tfA+tfA+tfA+tvI+tfA+tfA+tfAAAAA+tfAhX34AAAA+tfA+tfA+tfA+tvIAAAA+tfA+tfA+tvI+tfA+tfA+tvIhX34hX34hX34hX34hX34AAAA+tfAksitFAAAAa3RSTlMA9/na+kiPBernKJ/NnyrzOu4amz/bwGSioVVHJe3gTTxsC5Iu1ZcTA8q5KBYSyg716LqsilxFQhm9qJJ45NxjH7aDfm0HsV4E8X1vUeDUxb+0piIfC9isdXQyz3FTTTU05YXFjGlkVRxGL4BiVcwAAAaMSURBVFjD1ZdZV9pAFIAvAUQUBFlEkcoiEBAUCpRdFilLWRQQLHWrtXvx/z8394YEAiE9p6cv/R40mUlm+ObeWQLHw4PtFQ62nzOnMCdt3VimsOEAJFbLG415bwivfbWN1ecKW0CEojZHysizv/8TTstJobP2m6cxiFgK+0aRlMMWhWWKXEW+5gKRuEaW+Luz+zrMCBUZ1SpVQLolhuPqI16z5yoZtIDcWksmk4oR+ADZg3ln8Xd3z9cwY+ecmaMymdRWM0hwbVJNE0TevazjXT8iqk9XYRqA5PmbGkmUpjKgBLuhV0kKUULSW9z+VZDYnEpRbVpZmBO6YKjYOA/Rq5f13ESE16Yy7GGVX83f5CwAEN2Ve84NUMmb8EoiMUou9faqLStBGtUKiOgCfOG5WyLxKnkcDCaTyYNg8Physd1+FohmeA/J6fFl/VWObi6wZmM2wLtenB95qjkxmDA46hN6icu0UIF/SmXInfDkvsPppJ/g6d/wvX0qzyUYQ4MeNMzGpRAVA5EWYnohkbCXPRnPaDSKcP8SktEpjwFh/VtEHt9/b+ZvaOxxVDbxN1d9ACEXVbicBiwt8nc7PnCSvCll82+5EK7CB61shOc68vQwoN4+j0SJXQf/oN9WPKeXnWIgMIEpOXNbixLxs0Q/2QIoD+4oEHP617AIjag1BiIdExfWYpUrLdlAxB/GaDfFbt9PUaoWg7VEhjQL70WJzbT4tptSMW8RpgzedhoMJ9ZZnhPxU4DgSyJ43J5M2tt2odQDi1hRYt8yT08D11aDLWKqFEDEbMDBcgi3HymJT2KggIdGL9ESI+EGkQaDzc2sfCmMS6WL8WlElySOOInjlwyA5205M8oE43zxg6IEDZIX0j3uX8CyTqKziZIXoETrGHuzX8tJNHen1A3hOseMjsX2MFxNWQkP/onf3UO9zRe/UZII4RjpzcDu4+gU10kUGD67FLmnqf0oJ+Gn5Sqlo9g78Nqpgwv6KTo5iUOAbcyiMowTf5bYwiGyRgG8WH7lWyORpylhdHS8Ap3OLxhnJm9F7stJWmW/yklYerQtsHR9hU1XuD4CGH3zGokgTeg6eDR/ktClaFRwOw5Tv2skqlNiebNLyuyvbTmJGO1F7ysYey2tt7gSYvRVG0oSl9cwvvuThAXTs7eDOhe0hYdkJXQ5eYkDGYlnOQkfLQwNFy7pRvzpZuyoiY8ZKgoSdg+MgwoS8/TcoCjb9NixX15i759IVFHCZsKjgg7Qp4GPFRUkjg6hPtEoS0TD2HbHz+HSntAqK59OJ7Rbq8NzDAZux27bz+Z8PlqfTqyYTjGK+MYt9lmxTmkiKkuUlSQoPRGGmf89t8hK0F6ndoMi1Nurt3ISLj1/3hN2HNVCx/qmssRXZYlofvWc1tH99RI7Wb/EpsUl1jtdoaowJzLcTqEs8ZFZbTHgk5Pw0okupbzZbdOxWXazq5lmm92WzDrXM6+VuHmC8YHCxKb0XIVx62Qkbg107PCBAhk6dgxkjx30eikN4JQZN5V1/RKbhfGNooQrQDF2znB36D7vk5HQUeKZlM4d2QFNCckBUBqI/SiwBRynve6sy66TAqNm5192R3WAL6LEoNV61CienbQMbZ26GSGWImPakZEAbYk+OXLFrk3gB7QiGQ9P5s2XuzgdxU9FCVPxlp7r1nK71HA3BLYetuLUiThUWOMFOBLOq4Pk8EiTARjikBxC9k7xFBulBc66kCI2WjQufDISviIzJY2eWuAbZL/YBT7F+XHMLHzZ6emxUo9TQGo+CNWEaSewRduowQfbaCGgeQKY3JwFPRBJUkFitEbCjI0zthCIWKrUt0tGAtiaanWzG75IsT+2BIkVUixXvkdfVjBHV6QgpeHpYFHiITvyPDx6Mu3Ps0NsXSKRF3YdtkCjEltssckPGVdmC/BhnhNr7i1LjKQSmuQh8PhNywpXDuyIwrl5CwvY9LNV1nPzMudskEgOh4NLDX+bzIKEVECt7qWiXEfVklod8EoqK1dcWQkVbxs9denKKan1e405Q0A9o4TpdGTnubk8GxyXr8W9LVxSiwTCOaP3I/81hBm2H4IFoqlzriwMAG/sL2voR0DKTlqr7fp1AFGtW6tNs5JK3a0WiQHEzG6tO20BKazfnNYK/IB65PFwRubpeiHiPnxKIG3eYWfF5i53vwUSKlimxX3moa+RU3g9jMB/ROQ5cfnptYSju2R5DP8Vdc9k+0AC5uh/w2/WnX48g3gzxAAAAABJRU5ErkJggg==");
            LinkedResource res = new LinkedResource(new MemoryStream(imageData), "image/jpeg");
            res.ContentId = "imageId";
            res.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
            htmlView.LinkedResources.Add(res);
            mail.AlternateViews.Add(htmlView);

            using (var client = new System.Net.Mail.SmtpClient(configuration["SMTP:Name"], int.Parse(configuration["SMTP:Port"])))
            {
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(configuration["SMTP:Email"], configuration["SMTP:Password"]);
                client.EnableSsl = true;

                await client.SendMailAsync(mail);
            }
            await Task.Run(() => { });
        }
        

    }
}
