﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckYourTime.Services
{
    public interface IMailService
    {
        Task SendAsync(string toEmailAddress, string subject, string body);
    }
}
