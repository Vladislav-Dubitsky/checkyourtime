﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CheckYourTime.Services
{
    public class ParserService:IParser 
    {
        private readonly IConfiguration configuration;
        private readonly IMailService mailService;
        readonly ILoggerFactory loggerFactory;
        public ParserService(
            IConfiguration configuration,
            IMailService mailService,
            ILoggerFactory loggerFactory)
        {
            this.configuration = configuration;
            this.mailService = mailService;
            this.loggerFactory = loggerFactory;
        }
        public async Task<string> LogInAsync()
        {
            HttpClient client = new HttpClient();
            var credentials = new Dictionary<string, string>
            {
                { "login", configuration["LogIn:login"] },
                { "wachtwoord", configuration["LogIn:wachtwoord"] },
                { "submitButton", configuration["LogIn:submitButton"] }
            };
            var content = new FormUrlEncodedContent(credentials);

            var response = await client.PostAsync(configuration["LogIn:url"], content);
            if (response.IsSuccessStatusCode)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                return responseString;
            }
            return null;
        }
        public async Task CheckTime(IDictionary<string, DateTime> teamDates, IDictionary<string, string> members)
        {
            var dontUpdateTime = new List<String>();
            Console.WriteLine("Who doesn't update time:");
            foreach (var item in teamDates)
            {
                if (DateTime.UtcNow.Day > item.Value.Day)
                {
                    if (members.ContainsKey(item.Key))
                    {
                        Console.WriteLine($"Member:{item.Key}, email: {members[$"{item.Key}"]}");
                        dontUpdateTime.Add(item.Key);
                        var email = members[$"{item.Key}"] != null ? members[$"{item.Key}"] : null;
                        if (email != null)
                        {
                            var logger = loggerFactory.CreateLogger("FileLogger");
                            //await mailService.SendAsync(email, "Check your time", CreateCommonHtmlBody(item.Key));
                            logger.LogInformation($"Member:{item.Key}, email: {members[$"{item.Key}"]}");
                            logger.LogDebug($"Member:{item.Key}, email: {members[$"{item.Key}"]}");
                            
                        }
                    }
                }
            }
            //await mailService.SendAsync("eugen.bombela@tateeda.com", "Members who don't update time", CreateReportHtmlBody(dontUpdateTime));
        }

        public string CreateCommonHtmlBody(string name)
        {
            var logo = string.Format("<img src=\"cid:imageId\" />");
            var body = "<table style='width:100%'>" +
               "<tr><td align='left'>" +
                  logo +
               "<tr><td>" +
               "<p>"+$"{ name }, please make sure your time is up to date. It helps us and our clients to properly manage projects time." + "</p></td></tr>" +
               "<tr><td>Go to <a href='http://tateeda.10time.info/site/en/home/login/'>time tracker</a></td></tr>" +
               "</table>";

            return body;
        }
        public string CreateReportHtmlBody(List<String> members)
        {
            var logo = string.Format("<img src=\"cid:imageId\" />");

            var reportMembers = "";
            foreach (var item in members)
            {
                reportMembers += $"<li style='list-style:disc inside; mso-special-format:bullet;'>{item}</li>";
            }

            var body = "<table style='width:100%'>" +
                "<tr><td align='left'>" +
                  logo + "</td></tr>" +
               "<tr><td>" +
               "<p>Eugene, list of members, who don't update time </p></td></tr>" +
               "<tr><td align='left'><ul style='list-style-type: none'>" + reportMembers + "</ul></td></tr>" +
               "</table>";
            return body;
        }
    }
}
