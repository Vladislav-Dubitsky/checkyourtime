﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CheckYourTime.Services;
using FluentScheduler;
using Serilog.Extensions.Logging;
using Microsoft.Extensions.Logging;
using System.IO;

namespace CheckYourTime
{
    public class Startup
    {
        public static Dictionary<string, string> Members { get; private set; }
        public Startup(IConfiguration configuration, IScheduler scheduler)
        {
            //scheduler.ExecuteScheduleAsync();
            Configuration = configuration;
            Members = new Dictionary<string, string>();
            for (int i = 0; i <= 18; i++)
            {
                var name = Configuration[$"Team:{i}:name"];
                var email = Configuration[$"Team:{i}:email"];
                if (email != "" && email != null)
                {
                    Members.Add(name, email);
                }
            }
            JobManager.Initialize(scheduler.SetSchedulerAsync());
        }

        public IConfiguration Configuration { get; }
       

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "logger.txt"));
            //var logger = loggerFactory.CreateLogger("FileLogger");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
           
            app.UseMvc();
        }
    }

}
