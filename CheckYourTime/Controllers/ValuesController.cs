﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AngleSharp.Parser.Html;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using CheckYourTime.Services;
using System.Globalization;

namespace CheckYourTime.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly IParser parser;
        private Dictionary<string, DateTime> teamDates;
        public ValuesController (IParser parser)
        {
            this.parser = parser;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> ParsePage()
        {
            teamDates = new Dictionary<string, DateTime>();
            var result = await parser.LogInAsync();
            if (result != null) {
                var parser = new HtmlParser();
                var document = parser.Parse(result);
                var team = document.QuerySelectorAll("div#mwAll");
                foreach (var parent in team)
                {
                    foreach (var child in parent.Children)
                    {
                        teamDates.Add(child.LastElementChild.InnerHtml, Convert.ToDateTime(child.FirstElementChild.InnerHtml));
                    }
                }
            }
            await parser.CheckTime(teamDates, Startup.Members);
            return Ok(teamDates);
        }
    }
}
